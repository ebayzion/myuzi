INSTALL = install -D
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA = ${INSTALL} -m 644
UNINSTALL = rm -rf


install:
	@# python module
	python3 setup.py install --root="$(DESTDIR)"

	@# script file
	$(INSTALL_PROGRAM) bin/myuzi.py $(DESTDIR)/usr/bin/myuzi

	@# desktop file
	$(INSTALL_DATA) data/myuzi.desktop $(DESTDIR)/usr/share/applications/myuzi.desktop

	@# icons
	$(INSTALL_DATA) data/icons/scalable.svg $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/myuzi.svg
	$(INSTALL_DATA) data/icons/1024.png $(DESTDIR)/usr/share/icons/hicolor/1024x1024/apps/myuzi.png
	$(INSTALL_DATA) data/icons/128.png $(DESTDIR)/usr/share/icons/hicolor/128x128/apps/myuzi.png
	$(INSTALL_DATA) data/icons/16.png $(DESTDIR)/usr/share/icons/hicolor/16x16/apps/myuzi.png
	$(INSTALL_DATA) data/icons/192.png $(DESTDIR)/usr/share/icons/hicolor/192x192/apps/myuzi.png
	$(INSTALL_DATA) data/icons/22.png $(DESTDIR)/usr/share/icons/hicolor/22x22/apps/myuzi.png
	$(INSTALL_DATA) data/icons/24.png $(DESTDIR)/usr/share/icons/hicolor/24x24/apps/myuzi.png
	$(INSTALL_DATA) data/icons/256.png $(DESTDIR)/usr/share/icons/hicolor/256x256/apps/myuzi.png
	$(INSTALL_DATA) data/icons/32.png $(DESTDIR)/usr/share/icons/hicolor/32x32/apps/myuzi.png
	$(INSTALL_DATA) data/icons/36.png $(DESTDIR)/usr/share/icons/hicolor/36x36/apps/myuzi.png
	$(INSTALL_DATA) data/icons/384.png $(DESTDIR)/usr/share/icons/hicolor/384x384/apps/myuzi.png
	$(INSTALL_DATA) data/icons/48.png $(DESTDIR)/usr/share/icons/hicolor/48x48/apps/myuzi.png
	$(INSTALL_DATA) data/icons/512.png $(DESTDIR)/usr/share/icons/hicolor/512x512/apps/myuzi.png
	$(INSTALL_DATA) data/icons/64.png $(DESTDIR)/usr/share/icons/hicolor/64x64/apps/myuzi.png
	$(INSTALL_DATA) data/icons/72.png $(DESTDIR)/usr/share/icons/hicolor/72x72/apps/myuzi.png
	$(INSTALL_DATA) data/icons/96.png $(DESTDIR)/usr/share/icons/hicolor/96x96/apps/myuzi.png

	@# license
	$(INSTALL_DATA) LICENSE $(DESTDIR)/usr/share/licenses/myuzi/LICENSE


uninstall:
	@# python module
	pip uninstall -y myuzi

	@# script file
	$(UNINSTALL) $(DESTDIR)/usr/bin/myuzi

	@# desktop file
	$(UNINSTALL) $(DESTDIR)/usr/share/applications/myuzi.desktop

	@# icons
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/myuzi.svg
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/1024x1024/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/128x128/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/16x16/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/192x192/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/22x22/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/24x24/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/256x256/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/32x32/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/36x36/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/384x384/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/48x48/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/512x512/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/64x64/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/72x72/apps/myuzi.png
	$(UNINSTALL) $(DESTDIR)/usr/share/icons/hicolor/96x96/apps/myuzi.png

	@# license
	$(UNINSTALL) $(DESTDIR)/usr/share/licenses/myuzi/LICENSE


install-flatpak:
	@# python module
	python3 setup.py install --prefix="$(DESTDIR)"

	@# script file
	$(INSTALL_PROGRAM) bin/myuzi.py $(DESTDIR)/bin/myuzi

	@# desktop file
	sed -i -e 's%Icon=myuzi%Icon=com.gitlab.zehkira.Myuzi%g' data/myuzi.desktop
	sed -i -e 's%Exec=/usr/bin/myuzi%Exec=myuzi%g' data/myuzi.desktop
	$(INSTALL_DATA) data/myuzi.desktop $(DESTDIR)/share/applications/com.gitlab.zehkira.Myuzi.desktop

	@# icons
	$(INSTALL_DATA) data/icons/scalable.svg $(DESTDIR)/share/icons/hicolor/scalable/apps/com.gitlab.zehkira.Myuzi.svg

	@# license
	$(INSTALL_DATA) LICENSE $(DESTDIR)/usr/share/licenses/myuzi/LICENSE

