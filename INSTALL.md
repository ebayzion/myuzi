### Install package
- <a href='https://aur.archlinux.org/packages/myuzi'>Arch User Repository (Arch Linux)</a>
- <a href='https://gitlab.com/zehkira/myuzi/-/raw/master/flatpak/myuzi.flatpak?inline=false'>Flatpak (x86_64)</a>

### Install from source
Required packages:
- [yt-dlp](https://github.com/yt-dlp/yt-dlp)
- [xclip](https://github.com/astrand/xclip)
- [GStreamer](https://github.com/GStreamer/gstreamer)
    - [gst-libav](https://github.com/GStreamer/gst-libav)
    - [gst-plugins-good](https://github.com/GStreamer/gst-plugins-good)
- [Python 3](https://www.python.org/)
    - [BeautifulSoup4](https://pypi.org/project/beautifulsoup4/)
    - [PyGObject](https://pygobject.readthedocs.io/en/latest/getting_started.html#getting-started)
    - [Requests](https://pypi.org/project/requests/)
    - [Setuptools](https://pypi.org/project/setuptools/)

Clone or download this repository, and run the following command **as root**:
```sh
make install DESTDIR=/
```

To uninstall, run the following command **as root**:
```sh
make uninstall
```
