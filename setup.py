#!/usr/bin/env python3

import setuptools, sys


setuptools.setup(
	name = 'myuzi',
	version = '0.11.0',
	author = 'zehkira',
	description = 'Modules for Myuzi',
	url = 'https://gitlab.com/zehkira/myuzi',
	packages = setuptools.find_packages(),
	install_requires = ['requests', 'beautifulsoup4'],
	classifiers = [
		'Programming Language :: Python :: 3.6',
		'Operating System :: POSIX :: Linux'
	],
	python_requires = '>=3.6',
)
