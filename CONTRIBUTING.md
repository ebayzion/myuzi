## Issues

When reporting a bug, include the following information:
- your operating system,
- the installation method you used.

Always make sure you are running the latest version of the app before reporting a bug.

## Merge requests

Before starting work on a significant change, open an issue and explain what you want to do. This can prevent you from wasting time on a change that would not be accepted anyway.
